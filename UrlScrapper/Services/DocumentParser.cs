﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace UrlScrapper.Services
{
    public class DocumentParser
    {
        public string BaseUrl { get; set; }

        public DocumentParser(string urlString)
        {
            Uri uri = new Uri(urlString);
            BaseUrl = string.Format("{0}://{1}", uri.Scheme, uri.Authority);
        }

        public IEnumerable<string> GetImagesUrls(HtmlDocument doc)
        {
            return doc.DocumentNode.Descendants("img")
                .Select(e => GetAbsoluteImgSrc(e))
                .Where(s => !String.IsNullOrEmpty(s));
        }

        private string GetAbsoluteImgSrc(HtmlNode node)
        {
            var src = node.GetAttributeValue("src", null);
            if (String.IsNullOrEmpty(src))
                return null;
            // For relative image sources, append the base url
            if (!src.StartsWith("http"))
            {
                src = BaseUrl + src;
            }
            return src;
        }

        public IEnumerable<KeyValuePair<string, int>> GetWordsCount(HtmlDocument doc)
        {
            string docText = "";
            foreach (var node in doc.DocumentNode.SelectNodes("//text()"))
            {
                docText += node.InnerText;
            }
            // Words here are defined as a sequence of characters from Aa to Zz
            var wordRegex = new Regex(@"[^A-Za-z]");
            docText = wordRegex.Replace(docText, " ");
            char[] delimiters = new char[] { ' ' };
            var words = docText.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

            return words.GroupBy(w => w)
                .Select(w => new KeyValuePair<string, int>(w.Key, w.Count()))
                .OrderByDescending(x => x.Value);
        }
    }
}
