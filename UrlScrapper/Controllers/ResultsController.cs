using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HtmlAgilityPack;
using UrlScrapper.Models;

namespace UrlScrapper.Controllers
{
    public class ResultsController : Controller
    {
        // GET: Results
        public IActionResult Index(string urlString)
        {
            Uri uriResult;
            bool isValidUrl = Uri.TryCreate(urlString, UriKind.Absolute, out uriResult)
                && (uriResult.Scheme == "http" || uriResult.Scheme == "https");
            if (isValidUrl)
            {
                ScrappedDocument doc = new ScrappedDocument(urlString);
                return View(doc);
            }
            else
            {
                ModelState.AddModelError("InvalidUrl", "Not a valid URL format");
                return View(null);
            }
            
        }
    }
}