﻿using HtmlAgilityPack;
using System.Collections.Generic;
using UrlScrapper.Services;

namespace UrlScrapper.Models
{
    public class ScrappedDocument
    {
        public IEnumerable<string> ImageUrls { get; set; }
        public IEnumerable<KeyValuePair<string, int>> WordsCount { get; set; }

        public ScrappedDocument(string urlString)
        {
            var docParser = new DocumentParser(urlString);

            var web = new HtmlWeb();
            var doc = web.Load(urlString);

            ImageUrls = docParser.GetImagesUrls(doc);

            WordsCount = docParser.GetWordsCount(doc);       
        }

        
    }
}
